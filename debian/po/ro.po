# Mesajele în limba română pentru pachetul openldap.
# Romanian translation of openldap.
# Copyright © 2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the openldap package.
#
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
#
# Cronologia traducerii fișierului „openldap”:
# Traducerea inițială, făcută de R-GC, pentru versiunea openldap 2.5.13+dfsg-5(2021-08-16).
# Actualizare a traducerii pentru versiunea Y, făcută de X, Y(anul).
#
msgid ""
msgstr ""
"Project-Id-Version: openldap 2.5.13+dfsg-5\n"
"Report-Msgid-Bugs-To: openldap@packages.debian.org\n"
"POT-Creation-Date: 2021-08-16 01:12+0000\n"
"PO-Revision-Date: 2023-03-11 21:43+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <debian-l10n-romanian@lists.debian.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && "
"n%100<=19) ? 1 : 2);\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: Poedit 3.2.2\n"

#. Type: boolean
#. Description
#: ../slapd.templates:1001
msgid "Omit OpenLDAP server configuration?"
msgstr "Doriți să omiteți configurarea serverului OpenLDAP?"

#. Type: boolean
#. Description
#: ../slapd.templates:1001
msgid ""
"If you enable this option, no initial configuration or database will be created "
"for you."
msgstr ""
"Dacă activați această opțiune, nu va fi creată nicio configurație inițială sau "
"bază de date."

#. Type: select
#. Choices
#: ../slapd.templates:2001
msgid "always"
msgstr "întotdeauna"

#. Type: select
#. Choices
#: ../slapd.templates:2001
msgid "when needed"
msgstr "când este necesar"

#. Type: select
#. Choices
#: ../slapd.templates:2001
msgid "never"
msgstr "niciodată"

#. Type: select
#. Description
#: ../slapd.templates:2002
msgid "Dump databases to file on upgrade:"
msgstr "La actualizare, datele din baza de date vor fi salvate într-un fișier:"

#. Type: select
#. Description
#: ../slapd.templates:2002
msgid ""
"Before upgrading to a new version of the OpenLDAP server, the data from your "
"LDAP directories can be dumped into plain text files in the standard LDAP Data "
"Interchange Format."
msgstr ""
"Înainte de a face înnoirea la o nouă versiune a serverului OpenLDAP, datele din "
"directoarele dumneavoastră LDAP pot fi descărcate în fișiere text simplu în "
"formatul standard de interschimb de date LDAP (LDAP Data Interchange Format), "
"pe scurt, fișiere „LDIF”."

#. Type: select
#. Description
#: ../slapd.templates:2002
msgid ""
"Selecting \"always\" will cause the databases to be dumped unconditionally "
"before an upgrade. Selecting \"when needed\" will only dump the database if the "
"new version is incompatible with the old database format and it needs to be "
"reimported. If you select \"never\", no dump will be done."
msgstr ""
"Dacă selectați opțiunea „întotdeauna”, aceasta va face ca bazele de date să fie "
"descărcate necondiționat înainte de o înnoire. Dacă selectați opțiunea „când "
"este necesar”, aceasta va face ca bazele de date să fie descărcate numai dacă "
"noua versiune este incompatibilă cu vechiul format de bază de date și "
"informația conținută trebuie reimportată. Dacă selectați opțiunea „niciodată”, "
"nu se va face nicio descărcare."

#. Type: string
#. Description
#: ../slapd.templates:3001
msgid "Directory to use for dumped databases:"
msgstr "Directorul unde se descarcă/exportă bazele de date:"

#. Type: string
#. Description
#: ../slapd.templates:3001
msgid ""
"Please specify the directory where the LDAP databases will be exported. In this "
"directory, several LDIF files will be created which correspond to the search "
"bases located on the server. Make sure you have enough free space on the "
"partition where the directory is located. The first occurrence of the string "
"\"VERSION\" is replaced with the server version you are upgrading from."
msgstr ""
"Specificați directorul în care vor fi exportate bazele de date LDAP. În acest "
"director vor fi create mai multe fișiere „LDIF” care corespund bazelor de "
"căutare situate pe server. Asigurați-vă că aveți suficient spațiu liber pe "
"partiția în care se află directorul. Prima apariție a șirului „VERSION” este "
"înlocuită cu versiunea de server de la care faceți înnoirea."

#. Type: boolean
#. Description
#: ../slapd.templates:4001
msgid "Move old database?"
msgstr "Doriți să mutați baza de date veche?"

#. Type: boolean
#. Description
#: ../slapd.templates:4001
msgid ""
"There are still files in /var/lib/ldap which will probably break the "
"configuration process. If you enable this option, the maintainer scripts will "
"move the old database files out of the way before creating a new database."
msgstr ""
"Există încă fișiere în directorul „/var/lib/ldap” care probabil vor întrerupe "
"procesul de configurare. Dacă activați această opțiune, scripturile de "
"configurare vor muta fișierele vechi ale bazei de date înainte de a crea o nouă "
"bază de date."

#. Type: boolean
#. Description
#: ../slapd.templates:5001
msgid "Retry configuration?"
msgstr "Doriți să reîncercați din nou, operația de configurare?"

#. Type: boolean
#. Description
#: ../slapd.templates:5001
msgid ""
"The configuration you entered is invalid. Make sure that the DNS domain name is "
"syntactically valid, the field for the organization is not left empty and the "
"admin passwords match. If you decide not to retry the configuration the LDAP "
"server will not be set up. Run 'dpkg-reconfigure slapd' if you want to retry "
"later."
msgstr ""
"Configurația pe care ați introdus-o nu este validă. Asigurați-vă că numele de "
"domeniu DNS este valid din punct de vedere sintactic, câmpul pentru organizație "
"nu este lăsat gol și parolele de administrator se potrivesc. Dacă decideți să "
"nu reîncercați operația de configurare, serverul LDAP nu va fi configurat. "
"Rulați comanda «dpkg-reconfigure slapd» dacă doriți să reîncercați mai târziu."

#. Type: string
#. Description
#: ../slapd.templates:6001
msgid "DNS domain name:"
msgstr "Introduceți numele de domeniu DNS:"

#. Type: string
#. Description
#: ../slapd.templates:6001
msgid ""
"The DNS domain name is used to construct the base DN of the LDAP directory. For "
"example, 'foo.example.org' will create the directory with 'dc=foo, dc=example, "
"dc=org' as base DN."
msgstr ""
"Numele de domeniu DNS este folosit pentru a construi DN-ul de bază al "
"directorului LDAP. De exemplu, „foo.example.org” va crea directorul cu „dc=foo, "
"dc=example, dc=org” ca DN de bază."

#. Type: string
#. Description
#: ../slapd.templates:7001
msgid "Organization name:"
msgstr "Numele organizației:"

#. Type: string
#. Description
#: ../slapd.templates:7001
msgid ""
"Please enter the name of the organization to use in the base DN of your LDAP "
"directory."
msgstr ""
"Introduceți numele organizației de utilizat în DN-ul de bază al directorului "
"LDAP."

#. Type: password
#. Description
#: ../slapd.templates:8001
msgid "Administrator password:"
msgstr "Parola de administrator:"

#. Type: password
#. Description
#: ../slapd.templates:8001
msgid "Please enter the password for the admin entry in your LDAP directory."
msgstr ""
"Introduceți parola pentru intrarea de administrator din directorul "
"dumneavoastră LDAP."

#. Type: password
#. Description
#: ../slapd.templates:9001
msgid "Confirm password:"
msgstr "Confirmați parola:"

#. Type: password
#. Description
#: ../slapd.templates:9001
msgid ""
"Please enter the admin password for your LDAP directory again to verify that "
"you have typed it correctly."
msgstr ""
"Introduceți din nou parola de administrator pentru directorul dumneavoastră "
"LDAP pentru a verifica dacă ați introdus-o corect."

#. Type: note
#. Description
#: ../slapd.templates:10001
msgid "Password mismatch"
msgstr "Parolele nu se potrivesc"

#. Type: note
#. Description
#: ../slapd.templates:10001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""
"Cele două parole pe care le-ați introdus sunt diferite. Încercați din nou."

#. Type: boolean
#. Description
#: ../slapd.templates:11001
msgid "Do you want the database to be removed when slapd is purged?"
msgstr "Doriți ca baza de date să fie eliminată atunci când «slapd» este șters?"

#. Type: error
#. Description
#: ../slapd.templates:14001
msgid "slapcat failure during upgrade"
msgstr "«slapcat» a eșuat în timpul înnoirrii"

#. Type: error
#. Description
#: ../slapd.templates:14001
msgid "An error occurred while upgrading the LDAP directory."
msgstr "A apărut o eroare în timpul actualizării directorului LDAP."

#. Type: error
#. Description
#: ../slapd.templates:14001
msgid ""
"The 'slapcat' program failed while extracting the LDAP directory. This may be "
"caused by an incorrect configuration file (for example, missing 'moduleload' "
"lines to support the backend database)."
msgstr ""
"Programul «slapcat» a eșuat la extragerea directorului LDAP. Acest lucru poate "
"fi cauzat de un fișier de configurare incorect (de exemplu, lipsa liniilor "
"„moduleload” care să permită gestionarea diferitelor tipuri de baze de date)."

#. Type: error
#. Description
#. This paragraph is followed by a (non translatable) paragraph
#. containing a command line
#: ../slapd.templates:14001
msgid ""
"This failure will cause 'slapadd' to fail later as well. The old database files "
"will be moved to /var/backups. If you want to try this upgrade again, you "
"should move the old database files back into place, fix whatever caused slapcat "
"to fail, and run:"
msgstr ""
"Acest eșec va face ca «slapadd» să eșueze, de asemenea, mai târziu. Fișierele "
"vechi ale bazei de date vor fi mutate în directorul „/var/backups”. Dacă doriți "
"să încercați din nou această actualizare, ar trebui să mutați vechile fișiere "
"de baze de date înapoi la locul lor, să remediați orice a cauzat eșecul lui "
"«slapcat» și să rulați:"

#. Type: error
#. Description
#. Translators: keep "${location}" unchanged. This is a variable that
#. will be replaced by a directory name at execution
#: ../slapd.templates:14001
msgid ""
"Then move the database files back to a backup area and then try running slapadd "
"from ${location}."
msgstr ""
"Mutați fișierele bazei de date înapoi în directorul de copii de rezervă și apoi "
"încercați să rulați «slapadd» din ${location}."

#. Type: note
#. Description
#: ../slapd.templates:15001
msgid "Error while performing post-installation tasks"
msgstr "Eroare în timpul efectuării operațiilor de post-instalare"

#. Type: note
#. Description
#: ../slapd.templates:15001
msgid ""
"There has been one or more errors while performing some post-installation "
"tasks. This probably means that the slapd package could not automatically "
"migrate one or more LDAP databases, or that a backend being used by the current "
"OpenLDAP installation is not supported anymore."
msgstr ""
"A apărut una sau mai multe erori în timpul efectuării unor operații post-"
"instalare. Acest lucru înseamnă probabil că pachetul „slapd” nu a putut migra "
"automat una sau mai multe baze de date LDAP sau că un tip de bază de date "
"(mariadb/mysql, postgresql, etc.) utilizat de instalarea curentă OpenLDAP nu "
"mai este acceptat."

#. Type: note
#. Description
#: ../slapd.templates:15001
msgid ""
"The maintainer script responsible for executing the post-installation tasks has "
"exited, but the slapd service has NOT been (re)started. You will need to "
"manually fix the problem and then start the service."
msgstr ""
"Scriptul de configurare responsabil pentru executarea operațiilor de post-"
"instalare a ieșit, dar serviciul «slapd» NU a fost (re)pornit. Va trebui să "
"remediați manual problema și apoi să porniți serviciul."

#. Type: note
#. Description
#: ../slapd.templates:15001
msgid ""
"For more information on possible problematic scenarios and how to address them, "
"please take a look at the README.Debian file (under /usr/share/doc/slapd/)."
msgstr ""
"Pentru mai multe informații despre posibilele scenarii problematice și cum să "
"le soluționați, consultați fișierul „README.Debian” (din dosarul „/usr/share/"
"doc/slapd/”)."
